let today = new Date();
let day = today.getDay();
let daylist = ["Sunday","Monday","Tuesday","Wednesday ","Thursday","Friday","Saturday"];
let monthlist = ["January","February","March","April","May","June","July","August","September","October","November","December"];
let year = today.getFullYear();

let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();
let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
let dateTime = date+' '+time;
 
document.getElementById("displayDateTime").innerHTML =daylist[day]+' '+monthlist[day] + ' '+day+' '+year;

function showTime(){
    let date = new Date();
    let h = date.getHours(); // 0 - 23
    let m = date.getMinutes(); // 0 - 59
    let s = date.getSeconds(); // 0 - 59
    let session = "AM";
    
    if(h == 0){
        h = 12;
    }
    
    if(h > 12){
        h = h - 12;
        session = "PM";
    }
    
    h = (h < 10) ? "0" + h : h;
    m = (m < 10) ? "0" + m : m;
    s = (s < 10) ? "0" + s : s;
    
    var time = h + ":" + m + ":" + s + " " + session;
    document.getElementById("MyClockDisplay").innerText = time;
    document.getElementById("MyClockDisplay").textContent = time;
    
    setTimeout(showTime, 1000);
    
}

showTime();

// let timestart;
// let timestop;
let h1,m1,s1,h2,m2,s2;
function start(){
    // timestart=  new Date(Date.now());
    let day1 = new Date();
    h1 = day1.getHours(); // 0 - 23
    m1 = day1.getMinutes(); 
    s1 = day1.getSeconds(); // 0 - 59
    document.getElementById("startbutton").setAttribute("onclick","stop()")
    document.getElementById("myText").innerHTML = "Stop";
    document.getElementById("bg").style.backgroundColor = "grey";
    document.getElementById("img1").src="../img/stop-button.png";
    document.getElementById("start").innerHTML = day1.toLocaleTimeString();
    // document.getElementById("start").innerText = timestart.toLocaleTimeString([], { hour: "2-digit", minute: "2-digit",second:"2-digit" })
    
}
function stop(){
    timestop =  new Date(Date.now());
    let day2 = new Date();
    h2 = day2.getHours(); // 0 - 23
    m2 = day2.getMinutes(); 
    s2 = day2.getSeconds(); // 0 - 59
    document.getElementById("startbutton").setAttribute("onclick","myclear()");
    document.getElementById("myText").innerHTML = "Clear";
    document.getElementById("bg").style.backgroundColor = "brown";
    document.getElementById("img1").src="../img/clear.png";
    // document.getElementById("stop").innerHTML = h2 + ":" +m2 +":"+s2;
    document.getElementById("stop").innerHTML = day2.toLocaleTimeString();
    // document.getElementById("stop").innerText = timestart.toLocaleTimeString([], { hour: "2-digit", minute: "2-digit",second:"2-digit" })
    let totalh = (h2-h1)*60;
    let totalm = (m2-m1);
    let totals = (s2-s1)/60;
    let totalTime = totalh  + totalm  + totals;
    // let totalTime = 121;
    let money;
    if(totalTime<1 && totalTime>=0){
        totalTime = 0;
        money = 500 ;
    }
    else if(totalTime>=1 && totalTime<=15){
        money = 500;
    }
    else if(totalTime>15 && totalTime<=30){
        money=1000;
    }
    else if(totalTime>30 && totalTime<=60){
        money=1500;
    }
    else if(totalTime>=60){
        if(totalTime%60==0){
            money = (totalTime/60) *(1500);
        }
        else if(totalTime%60>0 && totalTime%60<=15){
            money = (((parseInt(totalTime/60))*1500)+500);
        }
        else if(totalTime%60>15 && totalTime%60<=30){
            money = (((parseInt(totalTime/60))*1500)+1000);
        }
        else if(totalTime%60>30 && totalTime%60<60){
            money = (((parseInt(totalTime/60))*1500)+1500);
        }
    }

    // console.log(61%60);
    

    
    document.getElementById("TotalMinutes").innerHTML = totalTime;
    document.getElementById("totalmoney").innerHTML = money;
  
    
    
    // alert("Clear");

    // totalTime();
}
// function totalTime(){
//     // let myStopTime = document.getElementById("stopTimes").children[2].textContent;
//     // let myStartTIme = ocument.getElementById("startTimes").children[2].textContent;
    
//     // let myTotal = myStopTime - myStartTIme;
//     let myTotal = h2-h1;
//     document.getElementById("TotalMinutes").innerHTML = myTotal;
//     alert(123)

// }
function myclear(){
    document.getElementById("startbutton").setAttribute("onclick","start()");
    document.getElementById("myText").innerHTML = "Start";
    document.getElementById("bg").style.backgroundColor = "#ef5350";
    document.getElementById("img1").src="../img/start.png";
    document.getElementById("start").innerHTML = "00:00:00";
    document.getElementById("stop").innerHTML = "00:00:00";
    document.getElementById("totalmoney").innerHTML = 0;
    document.getElementById("TotalMinutes").innerHTML = '';
    // document.getElementById("img1").style.backgroundImage = "url('./img/stop-button.png')";
    // alert(12)

}