tailwind.config = {
    theme: {
      extend: {
        colors: {
          clifford: '#da373d',
          text_color:'#e11d48',
        },
        backgroundImage:{
            banner:'url(../img/bg2.jpg)',
        },
        fontSize:{
            'w1' : '100%',
        },
        fontFamily: {
          lobster: ['"Lobster"', 'cursive'],
          tourney: ['"Tourney"', 'cursive'],
          philosopher: ['"Philosopher"', 'sans-serif'],
          aclonica: ['"Aclonica"', 'sans-serif'],
          
        }
      }
    }
  }